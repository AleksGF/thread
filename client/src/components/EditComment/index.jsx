import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

const EditComment = ({
  comment,
  editComment,
  toggleEditedComment
}) => {
  const { id: commentId, body: commentBody } = comment;
  const [body, setBody] = useState(commentBody);

  const handleEditComment = async () => {
    if (!body) {
      return;
    }
    await editComment(commentId, body);
    toggleEditedComment(undefined);
  };

  return (
    <Form reply onSubmit={handleEditComment}>
      <Form.TextArea
        value={body}
        onChange={ev => setBody(ev.target.value)}
      />
      <Button
        type="button"
        content="Cancel"
        labelPosition="left"
        icon="cancel"
        onClick={() => toggleEditedComment(undefined)}
      />
      <Button type="submit" content="Edit comment" labelPosition="left" icon="edit" primary />
    </Form>
  );
};

EditComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  editComment: PropTypes.func.isRequired,
  toggleEditedComment: PropTypes.func.isRequired
};

export default EditComment;
