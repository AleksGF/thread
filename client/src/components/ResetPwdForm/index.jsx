import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Segment, Message } from 'semantic-ui-react';

const ResetPwdForm = ({ resetPwd }) => {
  const [email, setEmail] = useState('');
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isResetDone, setIsResetDone] = useState(false);

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const handleResetClick = async () => {
    if (!isEmailValid) {
      return;
    }

    await resetPwd({ email });
    setIsResetDone(true);
  };

  return (
    <Form name="resetPwdForm" size="large" onSubmit={handleResetClick}>
      {!isResetDone && (
        <Segment>
          <Form.Input
            fluid
            icon="at"
            iconPosition="left"
            placeholder="Email"
            type="email"
            error={!isEmailValid}
            onChange={ev => emailChanged(ev.target.value)}
            onBlur={() => setIsEmailValid(validator.isEmail(email))}
          />
          <Button type="submit" color="teal" fluid size="large" primary>
            Reset your password
          </Button>
        </Segment>
      )}
      {isResetDone && (
        <Segment>
          <Message>
            Check your email for link to reset password
          </Message>
        </Segment>
      )}
    </Form>
  );
};

ResetPwdForm.propTypes = {
  resetPwd: PropTypes.func.isRequired
};

export default ResetPwdForm;
