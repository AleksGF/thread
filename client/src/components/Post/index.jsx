import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({
  post,
  likePost,
  toggleExpandedPost,
  toggleEditedPost,
  sharePost,
  userId,
  deletePost,
  toggleShowLikes,
  showLikes
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    postReactions
  } = post;
  const currentUser = userId;
  const date = moment(createdAt).fromNow();

  const makeListString = bool => {
    const resultArr = postReactions.filter(u => u.isLike === bool).map(i => i.user.username);
    const whoStr = resultArr.length ? resultArr.join(', ') : 'Nobody';
    const verbStr = bool ? 'like' : 'dislike';
    const verbEndStr = resultArr.length > 1 ? '' : 's';
    return `${whoStr} ${verbStr}${verbEndStr} this post`;
  };

  const usersWhoLikes = makeListString(true);
  const usersWhoDislikes = makeListString(false);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id, true)}>
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id, false)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleShowLikes(id)}>
          <Icon name="eye" />
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id, body)}>
          <Icon name="share alternate" />
        </Label>
        {(currentUser === user.id) && (
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleEditedPost(id)}>
            <Icon name="edit" />
          </Label>
        )}
        {(currentUser === user.id) && (
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => deletePost(id)}
          >
            <Icon name="delete" />
          </Label>
        )}
      </Card.Content>
      {showLikes && (
        <Card.Content extra>
          <hr />
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
            <Icon name="thumbs up" />
            {usersWhoLikes}
          </Label>
          <br />
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id, false)}>
            <Icon name="thumbs down" />
            {usersWhoDislikes}
          </Label>
        </Card.Content>
      )}
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  toggleShowLikes: PropTypes.func.isRequired,
  showLikes: PropTypes.string
};

Post.defaultProps = {
  showLikes: undefined
};

export default Post;
