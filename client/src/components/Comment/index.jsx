import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon, Divider } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({
  comment: { id, body, createdAt, user, likeCount, dislikeCount, commentReactions },
  currentUserId,
  likeComment,
  deleteComment,
  toggleEditedComment,
  toggleShowLikes,
  showLikes
}) => {
  const makeListString = bool => {
    const resultArr = commentReactions.filter(u => u.isLike === bool).map(i => i.user.username);
    const whoStr = resultArr.length ? resultArr.join(', ') : 'Nobody';
    const verbStr = bool ? 'like' : 'dislike';
    const verbEndStr = resultArr.length > 1 ? '' : 's';
    return `${whoStr} ${verbStr}${verbEndStr} this comment`;
  };

  const usersWhoLikes = makeListString(true);
  const usersWhoDislikes = makeListString(false);

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
      </CommentUI.Content>
      <CommentUI.Content>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => likeComment(id, true)}
        >
          <Icon name="thumbs up" />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => likeComment(id, false)}
        >
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleShowLikes(id)}>
          <Icon name="eye" />
        </Label>
        {(currentUserId === user.id) && (
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleEditedComment(id)}>
            <Icon name="edit" />
          </Label>
        )}
        {(currentUserId === user.id) && (
          <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deleteComment(id)}>
            <Icon name="delete" />
          </Label>
        )}
      </CommentUI.Content>
      {showLikes && (
        <CommentUI.Content>
          <Divider fitted />
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => likeComment(id, true)}
          >
            <Icon name="thumbs up" />
            {usersWhoLikes}
          </Label>
          <br />
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => likeComment(id, false)}
          >
            <Icon name="thumbs down" />
            {usersWhoDislikes}
          </Label>
        </CommentUI.Content>
      )}
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  currentUserId: PropTypes.string.isRequired,
  likeComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  toggleEditedComment: PropTypes.func.isRequired,
  toggleShowLikes: PropTypes.func.isRequired,
  showLikes: PropTypes.string
};

Comment.defaultProps = {
  showLikes: undefined
};

export default Comment;
