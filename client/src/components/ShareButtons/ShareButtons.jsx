import React from 'react';
import PropTypes from 'prop-types';
import { EmailShareButton, FacebookShareButton, TwitterShareButton,
  EmailIcon, FacebookIcon, TwitterIcon } from 'react-share';

import './styles.scss';

const ShareButtons = ({ postUrl, postBody }) => {
  const iconSize = 3 * parseFloat(
    getComputedStyle(document.documentElement).fontSize
  );

  return (
    <div className="shared-post-link__share-buttons_container">
      <EmailShareButton url={postUrl} subject="Sharing post" body={`${postBody}\n\nSource: `}>
        <EmailIcon size={iconSize} round />
      </EmailShareButton>
      <FacebookShareButton url={postUrl} quote={postBody}>
        <FacebookIcon size={iconSize} round />
      </FacebookShareButton>
      <TwitterShareButton url={postUrl} title={postBody}>
        <TwitterIcon size={iconSize} round />
      </TwitterShareButton>
    </div>
  );
};

ShareButtons.propTypes = {
  postUrl: PropTypes.string.isRequired,
  postBody: PropTypes.string.isRequired
};

export default ShareButtons;
