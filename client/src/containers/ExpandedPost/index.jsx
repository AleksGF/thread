import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import { likePost,
  toggleExpandedPost,
  toggleEditedPost,
  addComment,
  deletePost,
  likeComment,
  editComment,
  deleteComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';
import EditComment from 'src/components/EditComment';

const ExpandedPost = ({
  post,
  user,
  sharePost,
  deletePost: del,
  likePost: like,
  toggleExpandedPost: toggle,
  toggleEditedPost: toggleEdited,
  addComment: add,
  likeComment: likeCmnt,
  editComment: editCmnt,
  deleteComment: deleteCmnt,
  toggleShowLikes,
  showLikes
}) => {
  const [editCommentId, setEditCommentId] = useState(undefined);
  const [showCommentLikes, setShowCommentLikes] = useState(undefined);

  const toggleShowCommentLikes = id => {
    setShowCommentLikes(id === showCommentLikes ? undefined : id);
  };

  const toggleEditedCmnt = id => setEditCommentId(id);

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            <Post
              post={post}
              likePost={like}
              toggleExpandedPost={toggle}
              toggleEditedPost={toggleEdited}
              sharePost={sharePost}
              userId={user.id}
              deletePost={del}
              toggleShowLikes={toggleShowLikes}
              showLikes={showLikes === post.id ? showLikes : undefined}
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => {
                  if (comment.id === editCommentId) {
                    return (
                      <EditComment
                        key={comment.id}
                        comment={comment}
                        editComment={editCmnt}
                        toggleEditedComment={toggleEditedCmnt}
                      />
                    );
                  }
                  return (
                    <Comment
                      key={comment.id}
                      comment={comment}
                      currentUserId={user.id}
                      likeComment={likeCmnt}
                      editComment={editCmnt}
                      deleteComment={deleteCmnt}
                      toggleEditedComment={toggleEditedCmnt}
                      toggleShowLikes={toggleShowCommentLikes}
                      showLikes={showCommentLikes === comment.id ? showCommentLikes : undefined}
                    />
                  );
                })}
              <AddComment postId={post.id} addComment={add} />
            </CommentUI.Group>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  toggleShowLikes: PropTypes.func.isRequired,
  showLikes: PropTypes.string
};

ExpandedPost.defaultProps = {
  showLikes: undefined
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  user: rootState.profile.user
});

const actions = {
  likePost, toggleExpandedPost, toggleEditedPost, addComment, deletePost, likeComment, editComment, deleteComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
