import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import validator from 'validator';
import { setNewPassword } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header, Message, Form, Segment, Button } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

const ResetPage = ({ match, setNewPassword: setPwd }) => {
  const [password, setPassword] = useState('');
  const [passwordRepeat, setPasswordRepeat] = useState('');
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [isPasswordRepeatValid, setIsPasswordRepeatValid] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [isReseted, setIsReseted] = useState(false);

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const passwordRepeatChanged = data => {
    setPasswordRepeat(data);
    setIsPasswordRepeatValid(true);
  };

  const resetPassword = async () => {
    if (!isPasswordValid || !isPasswordRepeatValid || isLoading) {
      return;
    }

    setIsLoading(true);
    await setPwd({ password, token: match.params.hash });
    setIsLoading(false);
    setIsReseted(true);
  };

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        <Header as="h2" color="teal" textAlign="center">
          Enter new password
        </Header>
        <Form name="loginForm" size="large" onSubmit={resetPassword}>
          <Segment>
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              placeholder="Enter new password"
              type="password"
              error={!isPasswordValid}
              onChange={ev => passwordChanged(ev.target.value)}
              onBlur={() => setIsPasswordValid(Boolean(password))}
            />
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              placeholder="Repeat youe new password"
              type="password"
              error={!isPasswordRepeatValid}
              onChange={ev => passwordRepeatChanged(ev.target.value)}
              onBlur={() => setIsPasswordRepeatValid(validator.equals(passwordRepeat, password))}
            />
            <Button type="submit" color="teal" fluid size="large" loading={isLoading} disabled={isReseted} primary>
              Set new password
            </Button>
          </Segment>
        </Form>
        {isReseted && (
          <Message>
            Now you can sign in with your new password
            {' '}
            <NavLink exact to="/login">Sign In</NavLink>
          </Message>
        )}
      </Grid.Column>
    </Grid>
  );
};
ResetPage.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  setNewPassword: PropTypes.func.isRequired
};

const actions = { setNewPassword };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(ResetPage);
