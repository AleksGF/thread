import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { login, resetPassword } from 'src/containers/Profile/actions';
import Logo from 'src/components/Logo';
import { Grid, Header, Message, Segment } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';
import LoginForm from 'src/components/LoginForm';
import ResetPwdForm from 'src/components/ResetPwdForm';

const LoginPage = ({ login: signIn, resetPassword: reset }) => {
  const [isResetingPwd, setIsResetingPwd] = useState(false);

  const resetPwd = event => {
    event.preventDefault();
    setIsResetingPwd(true);
  };

  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Logo />
        {!isResetingPwd && (
          <Segment>
            <Header as="h2" color="teal" textAlign="center">
              Login to your account
            </Header>
            <LoginForm login={signIn} />
            <Message>
              Forgot your password?
              {' '}
              <NavLink to="/login" onClick={resetPwd}>Reset</NavLink>
              {' or '}
              New to us?
              {' '}
              <NavLink exact to="/registration">Sign Up</NavLink>
            </Message>
          </Segment>
        )}
        {isResetingPwd && (
          <Segment>
            <Header as="h2" color="teal" textAlign="center">
              Reset your password
            </Header>
            <ResetPwdForm resetPwd={reset} />
          </Segment>
        )}
      </Grid.Column>
    </Grid>
  );
};

LoginPage.propTypes = {
  login: PropTypes.func.isRequired,
  resetPassword: PropTypes.func.isRequired
};

const actions = { login, resetPassword };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(LoginPage);
