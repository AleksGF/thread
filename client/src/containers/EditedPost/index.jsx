import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Form, Button, Icon, Image, Segment } from 'semantic-ui-react';
import { toggleEditedPost } from 'src/containers/Thread/actions';
import Spinner from 'src/components/Spinner';

import styles from './styles.module.scss';

const EditedPost = ({
  post,
  toggleEditedPost: toggleEdited,
  editPost,
  uploadImage
}) => {
  const postImage = post.image ? { imageId: post.image.id, imageLink: post.image.link } : undefined;
  const [body, setBody] = useState(post.body);
  const [image, setImage] = useState(postImage);
  const [isUploading, setIsUploading] = useState(false);

  const handleEditPost = async () => {
    if (!body) {
      return;
    }
    const imageId = image ? image.imageId : null;
    await editPost({ id: post.id, imageId, body });
    toggleEdited();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggleEdited()}>
      {post
        ? (
          <Modal.Content>
            <Segment>
              <Form onSubmit={handleEditPost}>
                <Form.TextArea
                  name="body"
                  value={body}
                  onChange={ev => setBody(ev.target.value)}
                />
                {image?.imageLink && (
                  <div className={styles.imageWrapper}>
                    <Image className={styles.image} src={image?.imageLink} alt="post" />
                  </div>
                )}
                <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                  <Icon name="image" />
                  Attach new image
                  <input name="image" type="file" onChange={handleUploadFile} hidden />
                </Button>
                {image?.imageLink && (
                  <Button color="teal" onClick={() => setImage(undefined)} type="button">Delete current Image</Button>
                )}
                <Button floated="right" color="blue" type="submit">Save Post</Button>
              </Form>
            </Segment>
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

EditedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.editedPost
});

const actions = { toggleEditedPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditedPost);
