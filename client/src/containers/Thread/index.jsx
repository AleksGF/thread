/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import EditedPost from 'src/containers/EditedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Checkbox, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  toggleExpandedPost,
  toggleEditedPost,
  addPost,
  editPost,
  deletePost
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  from: 0,
  count: 10,
  showOwn: false,
  showExceptOwn: false,
  showLiked: false
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  editedPost,
  hasMorePosts,
  addPost: createPost,
  likePost: like,
  editPost: edit,
  deletePost: del,
  toggleExpandedPost: toggle,
  toggleEditedPost: toggleEdited
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [sharedPostBody, setSharedPostBody] = useState(undefined);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [showExceptOwnPosts, setShowExceptOwnPosts] = useState(false);
  const [showLikedPosts, setShowLikedPosts] = useState(false);
  const [showLikes, setShowLikes] = useState(undefined);

  const loadPostsWhenToggle = () => {
    postsFilter.userId = (
      postsFilter.showOwn || postsFilter.showExceptOwn || postsFilter.showLiked
    ) ? userId : undefined;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(!showOwnPosts);
    if (!showOwnPosts && showExceptOwnPosts) {
      setShowExceptOwnPosts(!showExceptOwnPosts);
      postsFilter.showExceptOwn = false;
    }
    postsFilter.showOwn = !showOwnPosts;
    loadPostsWhenToggle();
  };

  const toggleShowExceptOwnPosts = () => {
    setShowExceptOwnPosts(!showExceptOwnPosts);
    if (showOwnPosts && !showExceptOwnPosts) {
      setShowOwnPosts(!showOwnPosts);
      postsFilter.showOwn = false;
    }
    postsFilter.showExceptOwn = !showExceptOwnPosts;
    loadPostsWhenToggle();
  };

  const toggleShowLikedPosts = () => {
    setShowLikedPosts(!showLikedPosts);
    postsFilter.showLiked = !showLikedPosts;
    loadPostsWhenToggle();
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const toggleShowLikes = postId => {
    setShowLikes(postId === showLikes ? undefined : postId);
  };

  const sharePost = (id, body) => {
    setSharedPostId(id);
    setSharedPostBody(body);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Checkbox
          toggle
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Checkbox
          toggle
          label="Hide my posts"
          checked={showExceptOwnPosts}
          onChange={toggleShowExceptOwnPosts}
        />
        <Checkbox
          toggle
          label="Show only liked posts"
          checked={showLikedPosts}
          onChange={toggleShowLikedPosts}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={like}
            toggleExpandedPost={toggle}
            toggleEditedPost={toggleEdited}
            sharePost={sharePost}
            key={post.id}
            userId={userId}
            deletePost={del}
            toggleShowLikes={toggleShowLikes}
            showLikes={showLikes === post.id ? showLikes : undefined}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && (
        <ExpandedPost
          sharePost={sharePost}
          toggleShowLikes={toggleShowLikes}
          showLikes={showLikes}
        />
      )}
      {editedPost && <EditedPost editPost={edit} uploadImage={uploadImage} />}
      {sharedPostId && (
        <SharedPostLink
          postId={sharedPostId}
          postBody={sharedPostBody}
          close={() => setSharedPostId(undefined)}
        />
      )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  editedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  editedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  editedPost: rootState.posts.editedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  toggleExpandedPost,
  toggleEditedPost,
  addPost,
  editPost,
  deletePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
