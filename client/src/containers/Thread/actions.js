import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  EDIT_POST,
  DELETE_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SET_EDITED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const editPostAction = post => ({
  type: EDIT_POST,
  post
});

const deletePostAction = id => ({
  type: DELETE_POST,
  id
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setEditedPostAction = post => ({
  type: SET_EDITED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const editPost = post => async (dispatch, getRootState) => {
  const updatedPost = await postService.editPost(post);
  const { id } = updatedPost;
  const { posts: { expandedPost } } = getRootState();

  dispatch(editPostAction(updatedPost));

  if (expandedPost && expandedPost.id === id) {
    dispatch(setExpandedPostAction(updatedPost));
  }
};

export const deletePost = id => async (dispatch, getRootState) => {
  const { posts: { expandedPost } } = getRootState();
  await postService.deletePost(id);

  dispatch(deletePostAction(id));

  if (expandedPost && expandedPost.id === id) {
    dispatch(setExpandedPostAction(undefined));
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const toggleEditedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setEditedPostAction(post));
};

export const likePost = (postId, like = true) => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await postService.likePost(postId, like);
  let likesDiff = 0;
  let dislikesDiff = 0;
  const { profile: { user: { id: userId, username } } } = getRootState();

  if (like) {
    likesDiff = id ? 1 : -1;
    dislikesDiff = createdAt === updatedAt ? 0 : -1;
  } else {
    likesDiff = createdAt === updatedAt ? 0 : -1;
    dislikesDiff = id ? 1 : -1;
  }

  const mapReactions = reactions => {
    const userPreviousReaction = reactions.find(el => el.user.id === userId);
    let result = [...reactions];

    if (userPreviousReaction && userPreviousReaction.isLike === like) {
      result = reactions.filter(el => el.user.id !== userId);
    }

    if (userPreviousReaction && userPreviousReaction.isLike !== like) {
      result = reactions.map(el => {
        if (el.user.id === userId) {
          return { ...el, isLike: like };
        }
        return el;
      });
    }

    if (!userPreviousReaction) {
      result = [...reactions, { id: 'notKnownYet', isLike: like, user: { id: userId, username }, userId }];
    }

    return result;
  };

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likesDiff,
    dislikeCount: Number(post.dislikeCount) + dislikesDiff,
    postReactions: mapReactions(post.postReactions)
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const likeComment = (commentId, like) => async (dispatch, getRootState) => {
  const { id, createdAt, updatedAt } = await commentService.likeComment(commentId, like);
  let likesDiff = 0;
  let dislikesDiff = 0;
  const { profile: { user: { id: userId, username } } } = getRootState();

  if (like) {
    likesDiff = id ? 1 : -1;
    dislikesDiff = createdAt === updatedAt ? 0 : -1;
  } else {
    likesDiff = createdAt === updatedAt ? 0 : -1;
    dislikesDiff = id ? 1 : -1;
  }

  const mapReactions = reactions => {
    const userPreviousReaction = reactions.find(el => el.user.id === userId);
    let result = [...reactions];

    if (userPreviousReaction && userPreviousReaction.isLike === like) {
      result = reactions.filter(el => el.user.id !== userId);
    }

    if (userPreviousReaction && userPreviousReaction.isLike !== like) {
      result = reactions.map(el => {
        if (el.user.id === userId) {
          return { ...el, isLike: like };
        }
        return el;
      });
    }

    if (!userPreviousReaction) {
      result = [...reactions, { id: 'notKnownYet', isLike: like, user: { id: userId, username }, userId }];
    }

    return result;
  };

  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + likesDiff,
    dislikeCount: Number(comment.dislikeCount) + dislikesDiff,
    commentReactions: mapReactions(comment.commentReactions)
  });

  const { posts: { expandedPost } } = getRootState();
  const comments = expandedPost.comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));
  const updated = { ...expandedPost, comments };
  dispatch(setExpandedPostAction(updated));
};

export const editComment = (id, body) => async (dispatch, getRootState) => {
  const result = await commentService.editComment(id, body);

  if (result.ok) {
    const updatedComment = await commentService.getComment(id);
    const { posts: { expandedPost } } = getRootState();
    const comments = expandedPost.comments.map(comment => (comment.id !== id ? comment : updatedComment));
    const updated = { ...expandedPost, comments };
    dispatch(setExpandedPostAction(updated));
  }
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  const result = await commentService.deleteComment(commentId);

  if (result.ok) {
    const { posts: { posts, expandedPost } } = getRootState();
    const { id } = expandedPost;

    const mapComments = post => ({
      ...post,
      commentCount: Number(post.commentCount) - 1,
      comments: [...post.comments.filter(item => item.id !== commentId)]
    });

    const updated = mapComments(expandedPost);
    dispatch(setExpandedPostAction(updated));

    const updatedPosts = posts.map(item => (item.id !== id ? item : updated));
    dispatch(setPostsAction(updatedPosts));
  }
};
