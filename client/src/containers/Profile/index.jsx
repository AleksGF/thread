import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  Grid,
  Image,
  Input,
  Label,
  Icon,
  Button,
  Dimmer,
  Loader
} from 'semantic-ui-react';
import validator from 'validator';
import * as imageService from 'src/services/imageService';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { editUser } from './actions';

const Profile = ({ user, editUser: edit }) => {
  const [userData, setUserData] = useState(user);
  const [editedData, setEditedData] = useState(undefined);
  const [isEmailValid, setEmailValid] = useState(true);
  const [isUsernameValid, setUsernameValid] = useState(true);
  const [isUploading, setIsUploading] = useState(false);

  const uploadImage = file => imageService.uploadImage(file);

  const updateData = changes => {
    const updated = { ...userData, ...changes };
    setUserData(updated);
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      updateData({ imageId, image: { id: imageId, link: imageLink } });
      await edit(userData);
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  const editProfile = async ev => {
    const inputElement = ev.target.parentElement.parentElement.getElementsByTagName('input')[0];
    const inputName = inputElement.id;

    inputElement.focus();
    setEditedData(inputName);
    setUserData(user);
  };

  const cancelChanges = () => {
    setEditedData(undefined);
    setUserData(user);
  };

  const saveChanges = async () => {
    if (!isUsernameValid || !isEmailValid) {
      return;
    }

    setIsUploading(true);
    await edit(userData);
    setIsUploading(false);
    setEditedData(undefined);
    setUserData(userData);
  };

  const showBtn = action => {
    let func;
    let iconName;

    switch (action) {
      case 'edit':
        func = editProfile;
        iconName = 'edit';
        break;
      case 'cancel':
        func = cancelChanges;
        iconName = 'cancel';
        break;
      case 'save':
        func = saveChanges;
        iconName = 'save';
        break;
      default:
        func = null;
        iconName = '';
    }

    return (
      <Label transparent="true" size="small" as="a" onClick={func}>
        <Icon name={iconName} />
      </Label>
    );
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Dimmer active={isUploading} inverted>
          <Loader size="large" inline="centered" />
        </Dimmer>
        <Image centered src={getUserImgLink(userData.image)} size="medium" circular />
        <Button icon as="label" style={{ background: 'none', cursor: 'default' }}>
          <Label corner="right" style={{ opacity: 0.4, cursor: 'pointer' }}>
            <Icon name="upload" style={{ cursor: 'pointer' }} />
          </Label>
          <input name="image" type="file" onChange={handleUploadFile} hidden />
        </Button>
        <br />
        <Input
          id="status"
          fluid
          transparent
          labelPosition="right"
          placeholder="Status"
          type="text"
          disabled={editedData !== 'status'}
          onChange={ev => updateData({ status: ev.target.value })}
          value={userData.status || ''}
        >
          <Label transparent="true" size="small">
            <Icon name="info" />
          </Label>
          <input />
          {editedData !== 'status' && showBtn('edit')}
          {editedData === 'status' && showBtn('cancel')}
          {editedData === 'status' && showBtn('save')}
        </Input>
        <br />
        <br />
        <Input
          id="username"
          labelPosition="right"
          placeholder="Username"
          type="text"
          disabled={editedData !== 'username'}
          onChange={ev => updateData({ username: ev.target.value })}
          error={!isUsernameValid}
          onBlur={() => setUsernameValid(Boolean(userData.username))}
          value={userData.username}
        >
          <Label basic size="small">
            <Icon name="user" />
          </Label>
          <input />
          {editedData !== 'username' && showBtn('edit')}
          {editedData === 'username' && showBtn('cancel')}
          {editedData === 'username' && showBtn('save')}
        </Input>
        <br />
        <br />
        <Input
          id="email"
          labelPosition="right"
          placeholder="Email"
          type="email"
          disabled={editedData !== 'email'}
          onChange={ev => updateData({ email: ev.target.value })}
          error={!isEmailValid}
          onBlur={() => setEmailValid(validator.isEmail(userData.email))}
          value={userData.email}
        >
          <Label basic size="small">
            <Icon name="at" />
          </Label>
          <input />
          {editedData !== 'email' && showBtn('edit')}
          {editedData === 'email' && showBtn('cancel')}
          {editedData === 'email' && showBtn('save')}
        </Input>
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  editUser: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = {
  editUser
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
