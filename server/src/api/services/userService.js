import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, imageId, image, status } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image, status };
};

export const editUser = async (userId, data) => {
  const updatedUser = await userRepository.updateById(userId, data);
  const { id, username, email, status, imageId, image } = updatedUser;
  return { id, username, email, status, imageId, image };
};
