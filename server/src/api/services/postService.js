import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import { getUserById } from './userService';
import { makeTextForLike, sendEmail } from '../../helpers/emailHelper';

export const getPosts = async filter => {
  if (filter.showLiked === 'true') {
    const list = await postReactionRepository.getPostsWithLikes(filter.userId);
    const listLikedPosts = list.map(item => item.postId);

    return postRepository.getPosts(filter, listLikedPosts);
  }

  return postRepository.getPosts(filter);
};

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const edit = async (id, body) => {
  await postRepository.updateById(id, body);
  const result = postRepository.getPostById(id);
  return result;
};

export const deletePost = async id => {
  await postRepository.deleteById(id);
  return {};
};

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // send email to author when post was liked
  if (isLike && !Number.isInteger(result)) {
    const { username: whoLiked } = await getUserById(userId);
    const id = await postRepository.getPostById(postId).then(post => post.user).then(user => user.id);

    if (userId !== id) {
      const { email } = await getUserById(id);
      sendEmail({
        to: email,
        subject: 'Your post was liked',
        html: makeTextForLike(whoLiked)
      });
    }
  }
  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};
