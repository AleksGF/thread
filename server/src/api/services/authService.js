import { createToken, decodeToken } from '../../helpers/tokenHelper';
import { encrypt } from '../../helpers/cryptoHelper';
import { sendEmail, makeTextForReset } from '../../helpers/emailHelper';
import userRepository from '../../data/repositories/userRepository';

export const login = async ({ id }) => ({
  token: createToken({ id }),
  user: await userRepository.getUserById(id)
});

export const register = async ({ password, ...userData }) => {
  const newUser = await userRepository.addUser({
    ...userData,
    password: await encrypt(password)
  });
  return login(newUser);
};

export const resetPassword = async req => {
  const { email } = req;

  const user = await userRepository.getByEmail(email);

  if (!user) {
    return Promise.reject({ message: `Not found user with email: ${email}` });
  }

  const token = createToken({ id: user.id });
  sendEmail({
    to: email,
    subject: 'Reset your password',
    html: makeTextForReset(token)
  });

  return {};
};

export const setNewPassword = async req => {
  const { password: newPassword, token } = req;
  const { id } = decodeToken(token);

  const user = await userRepository.updateById(id, {
    password: await encrypt(newPassword)
  });

  if (!user) {
    return Promise.reject({ message: 'Invalid token' });
  }

  return user;
};
