import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const deleteComment = id => commentRepository.deleteById(id);

export const editComment = (id, body) => commentRepository.updateById(id, body);

export const setCommentReaction = async (userId, { id: commentId, like: isLike }) => {
  const updateOrDelete = react => (
    react.isLike === isLike
      ? commentReactionRepository.deleteById(react.id)
      : commentReactionRepository.updateById(react.id, { isLike })
  );

  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : commentReactionRepository.getCommentReaction(userId, commentId);
};
