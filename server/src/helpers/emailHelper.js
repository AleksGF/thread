import sgMail from '@sendgrid/mail';

export const sendEmail = async params => {
  // SENDGRID_API_KEY should be given in .env
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  const { to, subject, html } = params;
  // Email address should be given in .env
  const from = process.env.APP_EMAIL;

  const msg = {
    to,
    from,
    subject,
    html
  };

  sgMail.send(msg);
};

export const makeTextForReset = token => {
  const html = `
    <p>You are receiving this because you (or someone else) have requested the reset password for your account.</p>
    <p>Please click on the following link, or paste this into your browser to complete the process:</p>
    <br />
    <a href="http://localhost:3000/reset/${token}">http://localhost:3000/reset/${token}</a>
    <br />
    <p>If you did not request this, please ignore this email and your password will remain unchanged.</p>`;
  return html;
};

export const makeTextForLike = name => {
  const html = `
    <p>User ${name} liked your post!</p>
  `;
  return html;
};
