import { Op } from 'sequelize';
import sequelize from '../db/connection';
import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

// const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

const likeCase = bool => `(SELECT COUNT(*) FROM "postReactions" as "postReaction"
  WHERE "post"."id" = "postReaction"."postId" AND "postReaction"."isLike" = ${bool})`;
const likeCommentCase = bool => `(SELECT COUNT(*) FROM "commentReactions" as "commentReaction"
  WHERE "comments"."id" = "commentReaction"."commentId" AND "commentReaction"."isLike" = ${bool})`;

class PostRepository extends BaseRepository {
  async getPosts(filter, listLikedPosts = [null]) {
    const {
      from: offset,
      count: limit,
      userId,
      showOwn,
      showExceptOwn,
      showLiked
    } = filter;

    const where = {};

    if (userId && showOwn === 'true') {
      Object.assign(where, { userId }); // show own posts
    }

    if (userId && showExceptOwn === 'true') {
      Object.assign(where, { userId: { [Op.ne]: userId } }); // show except own posts
    }

    if (userId && showLiked === 'true') {
      Object.assign(where, { id: { [Op.or]: listLikedPosts } }); // show liked post
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.literal(likeCase(true)), 'likeCount'],
          [sequelize.literal(likeCase(false)), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: [],
        duplicating: false
      }, {
        model: PostReactionModel,
        attributes: ['id', 'userId', 'isLike'],
        include: {
          model: UserModel,
          attributes: ['id', 'username']
        },
        raw: true
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id',
        'postReactions.id',
        'postReactions->user.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id',
        'postReactions.id',
        'postReactions->user.id',
        'comments->commentReactions.id',
        'comments->commentReactions->user.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId"
                        AND "comment"."deletedAt" IS NULL)`), 'commentCount'],
          [sequelize.literal(likeCase(true)), 'likeCount'],
          [sequelize.literal(likeCase(false)), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        attributes: {
          include: [
            [sequelize.literal(likeCommentCase(true)), 'likeCount'],
            [sequelize.literal(likeCommentCase(false)), 'dislikeCount']
          ]
        },
        include: [
          {
            model: UserModel,
            attributes: ['id', 'username'],
            include: {
              model: ImageModel,
              attributes: ['id', 'link']
            }
          },
          {
            model: CommentReactionModel,
            attributes: []
          }, {
            model: CommentReactionModel,
            attributes: ['id', 'userId', 'isLike'],
            include: {
              model: UserModel,
              attributes: ['id', 'username'],
              raw: true
            }
          }
        ]
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }, {
        model: PostReactionModel,
        attributes: ['id', 'userId', 'isLike'],
        include: {
          model: UserModel,
          attributes: ['id', 'username']
        },
        raw: true
      }]
    });
  }
}

export default new PostRepository(PostModel);
