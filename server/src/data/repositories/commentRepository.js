import sequelize from '../db/connection';
import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

const likeCount = bool => `
  (SELECT COUNT(*) FROM "commentReactions" as "commentReaction"
  WHERE "comment"."id" = "commentReaction"."commentId" AND "commentReaction"."isLike" = ${bool})
`;

class CommentRepository extends BaseRepository {
  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id',
        'commentReactions.id',
        'commentReactions->user.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(likeCount(true)), 'likeCount'],
          [sequelize.literal(likeCount(false)), 'dislikeCount']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: CommentReactionModel,
        attributes: ['id', 'userId', 'isLike'],
        include: {
          model: UserModel,
          attributes: ['id', 'username'],
          raw: true
        }
      }]
    });
  }
}

export default new CommentRepository(CommentModel);
